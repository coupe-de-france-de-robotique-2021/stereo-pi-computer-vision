@echo off
SetLocal EnableDelayedExpansion
set content=
for /F "delims=" %%i in (ActiveCNN.txt) do set content=!content!%%i
echo Active CNN : %content%

tensorboard --logdir=models/%content%

EndLocal
pause