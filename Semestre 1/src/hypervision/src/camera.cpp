#include "camera.h"

Camera::Camera(string name) : _name(name), _deviceID(0), _apiID(CAP_ANY)
{    
    init();
}

Camera::Camera(string name, int deviceID, int apiID) : _name(name), _deviceID(deviceID), _apiID(apiID)
{
    init();
}

Camera::~Camera()
{
    // Void
}

void Camera::init()
{
    _capture = new VideoCapture(_deviceID, _apiID);
    
    _capture->set(CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
    _capture->set(CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);
    _capture->set(CAP_PROP_FPS, FPS_COUNT);
    
    if(!_capture->isOpened())
    {
        ROS_ERROR("Cannot open the video camera");
        return;
    }
    
    _frameWidth  = _capture->get(CAP_PROP_FRAME_WIDTH);
    _frameHeight = _capture->get(CAP_PROP_FRAME_HEIGHT);
    
    ROS_INFO_STREAM("Frame size : " << _frameWidth << " x " << _frameHeight);
}

bool Camera::grab(Mat &frame)
{       
    bool success = _capture->read(frame);

    if(!success)
    {
        return 0;
    }
    
    return 1;
}

void Camera::free(void)
{
    _capture->release();
}

string Camera::getName(void)
{
    return _name;
}

void Camera::setName(string newName)
{
    if(!newName.empty())
    {
        _name = newName;
    }
}

int Camera::getDeviceID(void)
{
    return _deviceID;
}

void Camera::setDeviceID(int newDeviceID)
{
    _deviceID = newDeviceID;
}

int Camera::getApiID(void)
{
    return _apiID;
}

void Camera::setApiID(int newApiID)
{
    _apiID = newApiID;
}
