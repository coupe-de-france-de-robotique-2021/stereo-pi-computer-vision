#include "topic.h"

string Topic::_message = "";

Topic::Topic(string name, int queueSize, ros::NodeHandle n) : _name(name), _queueSize(queueSize)
{
    _pub = n.advertise<std_msgs::String>(_name, _queueSize);
    _sub = n.subscribe(_name, _queueSize, subscriptionCallback);
    
    ROS_INFO_STREAM("Topic " << _name << " started with a queue size of " << _queueSize << " bytes ");
}

Topic::~Topic(void)
{
    // Void
}

void Topic::write(string msg)
{
    std_msgs::String message;
    
    std::stringstream ss;
    ss << msg;
    
    message.data = ss.str();
    
    ROS_INFO("%s", message.data.c_str());
    
    _pub.publish(message);
}

void Topic::subscriptionCallback(const std_msgs::String::ConstPtr& msg)
{
    ROS_INFO("Received : [%s]", msg->data.c_str());
    Topic::setMsg(msg->data.c_str());
}

string Topic::getMsg(void)
{
    return _message;
}

void Topic::setMsg(string newMessage)
{
    _message = newMessage;
}
