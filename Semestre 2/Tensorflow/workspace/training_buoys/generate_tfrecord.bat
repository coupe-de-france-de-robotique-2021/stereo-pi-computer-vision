:: Create train data:
python generate_tfrecord.py -x "images/train" -l "annotations/label_map.pbtxt" -o "annotations/train.record"

:: Create test data:
python generate_tfrecord.py -x "images/test" -l "annotations/label_map.pbtxt" -o "annotations/test.record"

:: For example
:: python generate_tfrecord.py -x C:/Users/sglvladi/Documents/Tensorflow/workspace/training_demo/images/train -l C:/Users/sglvladi/Documents/Tensorflow/workspace/training_demo/annotations/label_map.pbtxt -o C:/Users/sglvladi/Documents/Tensorflow/workspace/training_demo/annotations/train.record
:: python generate_tfrecord.py -x C:/Users/sglvladi/Documents/Tensorflow/workspace/training_demo/images/test -l C:/Users/sglvladi/Documents/Tensorflow2/workspace/training_demo/annotations/label_map.pbtxt -o C:/Users/sglvladi/Documents/Tensorflow/workspace/training_demo/annotations/test.record

pause