@echo off
SetLocal EnableDelayedExpansion
set content=
for /F "delims=" %%i in (ActiveCNN.txt) do set content=!content!%%i
echo Active CNN : %content%

python .\exporter_main_v2.py --input_type image_tensor --pipeline_config_path .\models\%content%\pipeline.config --trained_checkpoint_dir .\models\%content%\ --output_directory .\exported-models\%content%
copy .\annotations\label_map.pbtxt .\exported-models\%content%\saved_model

EndLocal
pause