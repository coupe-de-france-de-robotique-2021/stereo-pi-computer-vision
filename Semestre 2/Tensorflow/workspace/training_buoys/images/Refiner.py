import cv2
import numpy as np

from os import listdir
from os.path import isfile, join

IMG_HEIGHT = 1080
IMG_WIDTH = 1440

ratio = IMG_WIDTH / IMG_HEIGHT

def prepare_image(img) :
    h = img.shape[0]
    w = img.shape[1]

    r = w / h

    x1, x2 = 0, w
    y1, y2 = 0, h

    if r < ratio : #Means that h must be truncated
        ref = w / ratio
        y1 = int((h - ref) / 2)
        y2 = int(y1 + ref)
    else : #Means that w must be truncated
        ref = h * ratio
        x1 = int((w - ref) / 2)
        x2 = int(x1 + ref)

    img = img[y1:y2, x1:x2]
    #print(img.shape)

    img = cv2.resize(img,(IMG_WIDTH, IMG_HEIGHT))
    #print(img.shape)

    return img

# PARSE

files = [f for f in listdir("Raw") if isfile(join("Raw", f))]
nf = len(files)

st = int(input("Start counter from : "))
pad = int(input("Padding length : "))

for i in range(0,nf) :
    print("{} over {}, id : {}".format(i+1,nf,st+i))

    img = cv2.imread("Raw/" + files[i])
    img = prepare_image(img)

    name = "Sample_{index:0{padding}d}.png".format(index=st+i,padding=pad)
    print("Saved as " + name)
    
    cv2.imwrite("Fine/" + name, img)
