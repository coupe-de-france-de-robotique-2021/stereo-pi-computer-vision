@echo off
SetLocal EnableDelayedExpansion
set content=
for /F "delims=" %%i in (ActiveCNN.txt) do set content=!content!%%i
echo Active CNN : %content%

python model_main_tf2.py --model_dir=models/%content% --pipeline_config_path=models/%content%/pipeline.config

EndLocal
pause