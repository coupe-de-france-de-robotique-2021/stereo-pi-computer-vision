#include "hypervision.h"

Hypervision::Hypervision(string nodeName) : _nodeName(nodeName)
{
    // Void
}

Hypervision::Hypervision(string nodeName, int argc, char **argv) : _nodeName(nodeName)
{
    initialisation(argc, argv);
}

Hypervision::~Hypervision(void)
{
    // Void
}

void Hypervision::initialisation(int argc, char **argv)
{
    ros::init(argc, argv, _nodeName);
    
    _camLeft = new Camera("Left camera", 2, CAP_V4L2);
    _camRight = new Camera("Right camera", 4, CAP_V4L2);
    
    _net = new NeuralNet(0.8f, CAFFEE_PARAM, CAFFEE_MODEL);
    
    _node = new ros::NodeHandle;
    
    _topic = new Topic("Hypervision_topic", 1000, *_node);
    
    ROS_INFO("Starting Hypervision");
    
    #ifdef GUI
    initGUI();
    #endif // GUI
}

void Hypervision::initGUI(void)
{
    namedWindow(_camLeft->getName(), WINDOW_AUTOSIZE);
    namedWindow(_camRight->getName(), WINDOW_AUTOSIZE);
}

int Hypervision::exec(void)
{    
    ros::Rate loopRate(LOOP_RATE);
    
    while(ros::ok())
    {
        // Left
        Mat frameLeft;
        bool success = _camLeft->grab(frameLeft);
        if(success == 0)
        {
            ROS_ERROR("Cannot read a frame from video stream");
            return FAILED_READING_FROM_STREAM;
        }      
        
        _net->forward(frameLeft);
               
        imshow(_camLeft->getName(), frameLeft);
        
        // Right
        Mat frameRight;
        success = _camRight->grab(frameRight);
        if(success == 0)
        {
            ROS_ERROR("Cannot read a frame from video stream");
            return FAILED_READING_FROM_STREAM;
        }      
        
        _net->forward(frameRight);
               
        imshow(_camRight->getName(), frameRight);
        
        if(waitKey(30) == 27)
        {
            ROS_ERROR("Esc key pressed by user");
            break;
        }
    
        ros::spinOnce();
    
        loopRate.sleep();
    }
    
    _camLeft->free();
    _camRight->free();
    
    destroyWindow(_camLeft->getName());
    destroyWindow(_camRight->getName());
    
    ROS_INFO("Camera tested !");
    ROS_INFO("Shutting down...");
    ros::shutdown();
    
    return SUCCESS;
}

