#ifndef NEURALNET_H
#define NEURALNET_H

/*
    INCLUDES
*/
// ROS
#include <ros/ros.h>
// OpenCV
#include <opencv2/dnn/dnn.hpp>
#include <opencv2/imgproc/imgproc.hpp>
// Standards
#include <vector>
#include <cmath>
#include <string>

/*
    NAMESPACES
*/
using namespace std;
using namespace cv;

/***
  * NeuralNet class 
***/
class NeuralNet
{
    public:
        NeuralNet(void);
        NeuralNet(float confidence);
        NeuralNet(string pathToCaffeeParam, string pathToCaffeeModel);
        NeuralNet(float confidence, string pathToCaffeeParam, string pathToCaffeeModel);
        ~NeuralNet(void);
        
        void init(void);
        void loadFromCaffee(string pathToCaffeeParam, string pathToCaffeeModel);
        Mat detect(Mat img);
        void forward(Mat &img);
        void drawFromDetections(Mat img, Mat *detections);
        
    private:
        dnn::Net _net;
    
        float _confidence;
        
        const string _CLASSES[21] = {"Background",
                                  "Airplane",
                                  "Bike",
                                  "Bird",
                                  "Boat",
                                  "Bottle",
                                  "Bus",
                                  "Car",
                                  "Cat",
                                  "Chair",
                                  "Cow",
                                  "Table",
                                  "Dog",
                                  "Horse",
                                  "Motorbike",
                                  "Person",
                                  "Potted plant",
                                  "Sheep",
                                  "Sofa",
                                  "Train",
                                  "Monitor"};
                   
        const Scalar _COLORS[21] = {Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0),
                                 Scalar(0, 0, 0)};
};

#endif // NEURALNET_H
