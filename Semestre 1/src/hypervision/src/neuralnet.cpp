#include "neuralnet.h"

NeuralNet::NeuralNet() : _confidence(0.5f)
{
    // Void
}

NeuralNet::NeuralNet(float confidence) : _confidence(confidence)
{
    // Void
}

NeuralNet::NeuralNet(string pathToCaffeeParam, string pathToCaffeeModel)
{
    loadFromCaffee(pathToCaffeeParam, pathToCaffeeModel);
    //init();
}

NeuralNet::NeuralNet(float confidence, string pathToCaffeeParam, string pathToCaffeeModel) : _confidence(confidence)
{
    loadFromCaffee(pathToCaffeeParam, pathToCaffeeModel);
    //init();
}

NeuralNet::~NeuralNet(void)
{
    // Void
}

void NeuralNet::init(void)
{
    //_net.setPreferableBackend(dnn::DNN_BACKEND_INFERENCE_ENGINE);
    //_net.setPreferableTarget(dnn::DNN_TARGET_MYRIAD);
}

void NeuralNet::loadFromCaffee(string pathToCaffeeParam, string pathToCaffeeModel)
{
    _net = dnn::readNetFromCaffe(pathToCaffeeParam, pathToCaffeeModel);
    //_net = dnn::readNet(pathToCaffeeParam, pathToCaffeeModel);
    //_net = dnn::readNetFromModelOptimizer("/home/julien/mobilenet.xml", "/home/julien/mobilenet.bin");
}

Mat NeuralNet::detect(Mat img)
{
    Mat imgResized;
    resize(img, imgResized, Size(300, 300));
    
    Mat blob = dnn::blobFromImage(imgResized, 0.007843, Size(300, 300), 127.5);
    _net.setInput(blob);
    Mat d = _net.forward();
    
    return d; 
}

void NeuralNet::forward(Mat &img)
{
    Mat detections = detect(img);

    drawFromDetections(img, &detections);
}

void NeuralNet::drawFromDetections(Mat img, Mat *detections)
{

    for(int i = 0 ; i < detections->size[2] ; ++i)
    {
        float confidence = detections->at<float>(Vec<int, 4>(0, 0, i, 2));
        if(confidence > _confidence)
        {              
            int index = detections->at<float>(Vec<int, 4>(0, 0, i, 1));
            string label = _CLASSES[index] + " : " + to_string(floor(confidence * 10000) / 100);
            //ROS_INFO_STREAM(label);
            
            vector<int> box;
            box.reserve(4);
            box.push_back(detections->at<float>(Vec<int, 4>(0, 0, i, 3)) * 640); // StartX
            box.push_back(detections->at<float>(Vec<int, 4>(0, 0, i, 4)) * 480); // StartY
            box.push_back(detections->at<float>(Vec<int, 4>(0, 0, i, 5)) * 640); // EndX
            box.push_back(detections->at<float>(Vec<int, 4>(0, 0, i, 6)) * 480); // EndY
            
            int yLabel = 0;
            if(box[1] - 20 > 20)
            {
                yLabel = box[1] - 20;
            }
            
            else
            {
                yLabel = box[1] + 20;
            }
            
            rectangle(img, Point(box[0], box[1]), Point(box[2], box[3]), _COLORS[index], 2);
            putText(img, label, Point(box[1], yLabel), FONT_HERSHEY_SIMPLEX, 1.5, _COLORS[index], 3);
        }
    }
}

