# CMake generated Testfile for 
# Source directory: /home/julien/Softwares/ROS/vision_ws/src
# Build directory: /home/julien/Softwares/ROS/vision_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("interface")
subdirs("depthmap")
subdirs("hypervision")
