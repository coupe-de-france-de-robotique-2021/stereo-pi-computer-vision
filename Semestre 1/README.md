Computer Vision protocol

This file is going to show you how to download, compile and use the StereoPI Computer Vision program part from the Robotic French Cup robot. We suppose you have ROS installed under Linux with OpenCV dependencies.

This program has been tested with : 
    Personnal computer : Linux Ubuntu 20.04 64 bits (Focal)
    Raspberry PI Compute Module 3 : Raspberry PI OS Lite December 2020 (Buster)

Let's get started !

First, clone the repository :

    cd ~/
    git clone https://framagit.org/coupe-de-france-de-robotique-2021/stereo-pi-computer-vision.git

Then, setup the global variable for the shell :

    cd ~/stereo-pi-computer-vision
    source devel/setup.sh

You are now able to launch the nodes, make sure everything is compiled :

    cd ~/stereo-pi-computer-vision
    catkin_make

Enjoy !

To run the node which uses two cameras and detect objects from stream :

    rosrun hypervision hypervision_node

To run the node which generate a depthmap from the two cameras : (In development)

    rosrun depthmap depthmap_node

To run the node which communicate with a subscriber and a publisher from the master to the two other nodes : (In development)

    rosrun interface interface_node

If one of the following error appears :
    "Cannot read a frame from video stream" 
    "Cannot open the video camera" 
You should check your camera connection and / or the deviceID and apiID in the src/hypervision.cpp file :
    _camLeft = new Camera("Left camera", 2, CAP_V4L2);   [Line 22]
    _camRight = new Camera("Right camera", 4, CAP_V4L2); [Line 23]
The "2" and "4" parameters sent to the Camera constructor are the deviceID and the CAP_V4L2 are the apiID. You have to adapt these values to your configuration.

Under Linux, you can find the deviceID using this command line :
    ls -l /dev/video*
You should have a result like this :
    crw-rw----+ 1 root video 81, 0 déc.  19 14:39 /dev/video0
    crw-rw----+ 1 root video 81, 1 déc.  19 14:39 /dev/video1
    crw-rw----+ 1 root video 81, 2 déc.  19 15:34 /dev/video2
    crw-rw----+ 1 root video 81, 3 déc.  19 15:34 /dev/video3
    crw-rw----+ 1 root video 81, 4 déc.  19 15:35 /dev/video4
    crw-rw----+ 1 root video 81, 5 déc.  19 15:35 /dev/video5
    ...
The last number is the deviceID.

Do not forget to recompile after every modification of the sources !
    catkin_make