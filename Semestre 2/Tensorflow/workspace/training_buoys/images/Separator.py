import os
from os import listdir
from os.path import isfile, join
import shutil
import random

files = [f for f in listdir("Fine") if isfile(join("Fine", f))]
nf = int(len(files)/2)

pad = 4

train = 0
test = 0

for i in range(1,nf+1) :
    print("{} over {} : ".format(i,nf), end='')

    if random.random() > 0.15 :
        train += 1
        shutil.copy("Fine/Sample_{index:0{padding}d}.png".format(index=i,padding=pad), "train")
        shutil.copy("Fine/Sample_{index:0{padding}d}.xml".format(index=i,padding=pad),"train")
        print("train")
    else :
        test += 1
        shutil.copy("Fine/Sample_{index:0{padding}d}.png".format(index=i,padding=pad), "test")
        shutil.copy("Fine/Sample_{index:0{padding}d}.xml".format(index=i,padding=pad), "test")
        print("test")

print("Train ratio : ", train/nf)
print("Test ratio : ", test/nf)
