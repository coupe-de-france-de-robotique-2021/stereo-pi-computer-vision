/*
    INCLUDES
*/
// ROS
#include <ros/ros.h>

// Classes
#include "topic.h"

/*
    GLOBAL CONSTANTS
*/
#define SUCCESS 0
#define FAILED -1
#define LOOP_RATE 100

/***
  * main function
***/
int main(int argc, char **argv)
{
    // Initialisation
    ros::init(argc, argv, "Interface");
    ros::NodeHandle n;
    
    Topic *topic = new Topic("Hypervision_topic", 1000, n);

    ros::Rate loopRate(LOOP_RATE);
    
    while(ros::ok())
    {
        string cmd;
        
        cout << "Hypervision_topic : ";
        getline(cin, cmd);
        //cout << "You entered : " << cmd << endl;
        
        if(cmd.compare("exit") == 0 || cmd.compare("q") == 0)
        {
            break;
        }
        
        else
        {
            topic->write(cmd);
        }
                
        ros::spinOnce();
    
        loopRate.sleep();
    }
    
    ROS_INFO("Interface tested !");
    ROS_INFO("Shutting down...");
    ros::shutdown();
    
    return SUCCESS;
}

