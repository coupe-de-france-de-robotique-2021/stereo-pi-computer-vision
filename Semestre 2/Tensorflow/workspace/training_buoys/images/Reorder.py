import os
from os import listdir
from os.path import isfile, join

start_old = int(input("Current start : "))
start_new = int(input("New start : "))
pad = int(input("Old padding : "))
pad_new = int(input("New padding : "))

files = [f for f in listdir("Fine") if isfile(join("Fine", f))]
nf = int(len(files)/2)

for i in range(0, nf) :
    print("Sample_{index_old:0{padding_old}d} to Sample_{index_new:0{padding_new}d}".format(index_old=start_old+i,index_new=start_new+i,padding_old=pad,padding_new=pad_new))
    os.rename("Fine/Sample_{index_old:0{padding_old}d}.png".format(index_old=start_old+i,padding_old=pad), "Fine/Sample_{index_new:0{padding_new}d}.png".format(index_new=start_new+i,padding_new=pad_new))
    os.rename("Fine/Sample_{index_old:0{padding_old}d}.xml".format(index_old=start_old+i,padding_old=pad), "Fine/Sample_{index_new:0{padding_new}d}.xml".format(index_new=start_new+i,padding_new=pad_new))
