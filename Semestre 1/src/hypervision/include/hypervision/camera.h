#ifndef CAMERA_H
#define CAMERA_H

/*
    INCLUDES
*/
// ROS
#include <ros/ros.h>
// OpenCV
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
// Standards
#include <string>

/*
    GLOBAL CONSTANTS
*/
#define FRAME_WIDTH 640
#define FRAME_HEIGHT 480
#define FPS_COUNT 30

/*
    NAMESPACES
*/
using namespace std;
using namespace cv;

/***
  * Camera Class
***/
class Camera
{
    public:
        Camera(string name);
        Camera(string name, int deviceID, int apiID);
        ~Camera(void);
        
        bool grab(Mat &frame);
        void free(void);
        
        // Accessors
        string getName(void);
        int getDeviceID(void);
        int getApiID(void);
        
        // Mutators
        void setName(string newName);
        void setDeviceID(int newDeviceID);
        void setApiID(int newApiID);
        
    private:
        void init(void);
        
    private:
        string _name;
        int _deviceID;
        int _apiID;
        
        VideoCapture *_capture;
        
        double _frameWidth;
        double _frameHeight;
};

#endif // CAMERA_H
