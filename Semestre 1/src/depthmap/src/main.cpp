/*
    INCLUDES
*/
// ROS
#include <ros/ros.h>
// OpenCV
#include <opencv2/highgui/highgui.hpp>
// Classes
#include "camera.h"
#include "topic.h"

/*
    GLOBAL CONSTANTS
*/
#define SUCCESS 0
#define FAILED -1
#define FAILED_READING_FROM_STREAM -2
#define LOOP_RATE 100

using namespace cv;

/***
  * main function
***/
int main(int argc, char **argv)
{
    // Initialisation
    ros::init(argc, argv, "Depthmap");
    ros::NodeHandle n;
    Topic *topic = new Topic("Depthmap", 1000, n);

    Camera *_camLeft = new Camera("Left camera", 2, CAP_V4L2);
    Camera *_camRight = new Camera("Right camera", 4, CAP_V4L2);
    
    namedWindow(_camLeft->getName(), WINDOW_AUTOSIZE);
    namedWindow(_camRight->getName(), WINDOW_AUTOSIZE);

    ros::Rate loopRate(LOOP_RATE);
    
    while(ros::ok())
    {
        Mat frameLeft;
        bool success = _camLeft->grab(frameLeft);
        if(success == 0)
        {
            ROS_ERROR("Cannot read a frame from video stream");
            return FAILED_READING_FROM_STREAM;
        }
        
        Mat frameRight;
        success = _camRight->grab(frameRight);
        if(success == 0)
        {
            ROS_ERROR("Cannot read a frame from video stream");
            return FAILED_READING_FROM_STREAM;
        }
              
        imshow(_camLeft->getName(), frameLeft);
        imshow(_camRight->getName(), frameRight);
        
        if(waitKey(30) == 27)
        {
            ROS_ERROR("Esc key pressed by user");
            break;
        }

        ros::spinOnce();
    
        loopRate.sleep();
    }
    
    _camLeft->free();
    _camRight->free();
    
    destroyWindow(_camLeft->getName());
    destroyWindow(_camRight->getName());
    
    ROS_INFO("Camera tested !");
    ROS_INFO("Shutting down...");
    ros::shutdown();
    
    return SUCCESS;
}

