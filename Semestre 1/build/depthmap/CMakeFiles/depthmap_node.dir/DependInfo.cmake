# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/julien/Softwares/ROS/vision_ws/src/depthmap/src/camera.cpp" "/home/julien/Softwares/ROS/vision_ws/build/depthmap/CMakeFiles/depthmap_node.dir/src/camera.cpp.o"
  "/home/julien/Softwares/ROS/vision_ws/src/depthmap/src/main.cpp" "/home/julien/Softwares/ROS/vision_ws/build/depthmap/CMakeFiles/depthmap_node.dir/src/main.cpp.o"
  "/home/julien/Softwares/ROS/vision_ws/src/depthmap/src/topic.cpp" "/home/julien/Softwares/ROS/vision_ws/build/depthmap/CMakeFiles/depthmap_node.dir/src/topic.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"depthmap\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/julien/Softwares/ROS/vision_ws/src/depthmap/include/depthmap"
  "/opt/ros/noetic/include"
  "/opt/ros/noetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/opencv4"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
