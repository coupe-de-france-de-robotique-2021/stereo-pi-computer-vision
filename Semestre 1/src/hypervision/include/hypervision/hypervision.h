#ifndef CONFIG_H
#define CONFIG_H

/*
    INCLUDES
*/
// ROS
#include <ros/ros.h>
// OpenCV
#include <opencv2/highgui/highgui.hpp>
//Standards
#include <string>
// Classes
#include "camera.h"
#include "neuralnet.h"
#include "topic.h"

/*
    GLOBAL CONSTANTS
*/
#define SUCCESS 0
#define FAILED -1
#define FAILED_READING_FROM_STREAM -2

#define CAFFEE_MODEL "data/MobileNetSSD_deploy.caffemodel"
#define CAFFEE_PARAM "data/MobileNetSSD_deploy.prototxt.txt" 

#define LOOP_RATE 60

#define GUI

class Hypervision
{
    public:
        Hypervision(string nodeName);
        Hypervision(string nodeName, int argc, char **argv);
        ~Hypervision(void);
        
        void initialisation(int argc, char **argv);
        void initGUI(void);
        
        int exec(void);
        
    private:
        string _nodeName;
        ros::NodeHandle *_node;
        
        Camera *_camLeft;
        Camera *_camRight;
        
        NeuralNet *_net;
        
        Topic *_topic;
};

#endif // CONFIG_H
