/*
    INCLUDES
*/
#include "hypervision.h"

/***
  * main function
***/
int main(int argc, char **argv)
{                              
    Hypervision hpv("Hypervision", argc, argv);
    
	return hpv.exec();
}

