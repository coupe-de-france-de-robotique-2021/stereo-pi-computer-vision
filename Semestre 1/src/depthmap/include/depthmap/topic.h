#ifndef TOPIC_H
#define TOPIC_H

/*
    INCLUDES
*/
// ROS
#include <ros/ros.h>
#include <std_msgs/String.h>
// Standards
#include <string>

using namespace std;

class Topic
{
    public:
        Topic(string name, int queueSize, ros::NodeHandle n);
        ~Topic(void);
        
        void write(string msg);
        
        static void subscriptionCallback(const std_msgs::String::ConstPtr& msg);
        static string getMsg(void);
        static void setMsg(string newState);
        
    private:
        string _name;
        int _queueSize;
        
        ros::Publisher _pub;
        
        ros::Subscriber _sub;
        
        static string _message;
};

#endif // TOPIC_H
